<?php
$entrada       = [55, 58, 60];
$completeRange = new Completerange();
$resultado     = $completeRange->build($entrada);

foreach ($resultado as $value) {
    echo $value. " ";
}

class Completerange
{          

    public function build($entrada)
    {
        $firstValue = array_shift($entrada);
        $lastValue = end($entrada);
        $array = [];

        for ($i=$firstValue; $i <= $lastValue; $i++) { 
            array_push($array, $i);
        }

        return $array;
    }
}
?>
 