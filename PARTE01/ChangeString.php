<?php
$cadena       = "**Casa 52Z";
$changeString = new Changestring();
$resultado    = $changeString->build($cadena);
echo $resultado;

class Changestring
{          

    public function build($cadena)
    {
        $abecedario = ["a", "b", "c", "d", "e", "f", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

        $array = str_split($cadena);
        $cadenaNueva = [];

        foreach ($array as $value) {

            if(ctype_alpha ($value)) {
                if (ctype_upper($value)) { 
                    $key = array_search(strtolower($value), $abecedario) + 1;
                    if(array_key_exists($key, $abecedario)){
                        array_push($cadenaNueva, strtoupper($abecedario[$key]));
                    } else {
                        array_push($cadenaNueva, strtoupper($abecedario[0]));
                    }
                } else {
                    $key = array_search($value, $abecedario) + 1;
                    if(array_key_exists($key, $abecedario)){
                        array_push($cadenaNueva, $abecedario[$key]);
                    } else {
                        array_push($cadenaNueva, $abecedario[0]);
                    }
                }
            } else {
                array_push($cadenaNueva, $value);
            }
        }
        return implode($cadenaNueva);
    }
}
?>
 