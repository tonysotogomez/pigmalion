<?php
$entrada   = ")(";
$clearPar  = new Clearpar();
$resultado = $clearPar->build($entrada);
echo $resultado;

class Clearpar
{          
    public function build($entrada)
    {
        $array = str_split($entrada);
        $newArray = [];
        $ind = 1; // 1 = open, 0 = close
        $count = count($array) - 1;

        foreach ($array as $key => $value) {
            if ($value == "(" && $ind == 1 && $key != $count) {
                array_push($newArray, $value);
                $ind = 0;
            }
            if ($value == ")" && $ind == 0) {
                array_push($newArray, $value);
                $ind = 1;
            }
        }

        return implode($newArray);
    }
}
?>
 