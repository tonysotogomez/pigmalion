<?php
// Routes

$app->get('/QUIT[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});


$app->get('/', function ($request, $response, $args) {
    $data = file_get_contents("../employees.json");
	$args['employees'] = json_decode($data, true);

    return $this->renderer->render($response, 'listado.phtml', $args);
});


$app->get('/buscar', function ($request, $response, $args) {
  	$args['email'] = $request->getParam('email');

  	$data = file_get_contents("../employees.json");
  	$employees = json_decode($data, true);

  	$con = 0;
  	if (trim($args['email']) == "") {
  		$args['employees'] = $employees;
  	} else {
	  	foreach ($employees as $e) {
	  		if($e['email'] == $args['email']) {
					$employee[$con]['name']     = $e['name'];
					$employee[$con]['email']    = $e['email'];
					$employee[$con]['position'] = $e['position'];
					$employee[$con]['salary']   = $e['salary'];
					$employee[$con]['id']   	= $e['id'];
	  			$con++;
	  		}
	  	}
	  	$args['employees'] = $employee;
  	}
  	
    return $this->renderer->render($response, 'busqueda.phtml', $args);
});

$app->get('/detalle/{id}', function ($request, $response, $args) {

  	$data = file_get_contents("../employees.json");
  	$employees = json_decode($data, true);

  	foreach ($employees as $e) {
	  		if($e['id'] == $args['id']) {
					$employee['name']     = $e['name'];
					$employee['email']    = $e['email'];
					$employee['phone']    = $e['phone'];
					$employee['address']  = $e['address'];
					$employee['position'] = $e['position'];
					$employee['salary']   = $e['salary'];
					$employee['skills']   = $e['skills'];
	  		}
	  	}
	$args['employee'] = $employee;

    return $this->renderer->render($response, 'detalle.phtml', $args);
});


$app->post('/servicio', function ($request) {
	$params = $request->getParsedBody();
	$data = file_get_contents("../employees.json");
  	$employeesArray = json_decode($data, true);
  	$employees = [];

  	$con = 0;
  	foreach ($employeesArray as $e) {

  		$salary = str_replace('$','',str_replace(',','',$e['salary']));

  		if($salary >= $params['min'] && $salary <= $params['max']) {
					$employees[$con]['name']     = $e['name'];
					$employees[$con]['email']    = $e['email'];
					$employees[$con]['phone']    = $e['phone'];
					$employees[$con]['address']  = $e['address'];
					$employees[$con]['position'] = $e['position'];
					$employees[$con]['salary']   = $e['salary'];
					$employees[$con]['id']   	 = $e['id'];
					$employees[$con]['skills']   = $e['skills'];
  			$con++;
  		}
  	}

	function array_to_xml( $data, &$xml_data ) {
	    foreach( $data as $key => $value ) {
	        if( is_numeric($key) ){
	            $key = 'item'.$key;
	        }
	        if( is_array($value) ) {
	            $subnode = $xml_data->addChild($key);
	            array_to_xml($value, $subnode);
	        } else {
	            $xml_data->addChild("$key",htmlspecialchars("$value"));
	        }
	     }
	}

	$xml = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
	array_to_xml($employees,$xml);
	return $xml->asXML();


});